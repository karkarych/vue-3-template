import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueCookies from "vue-cookies";
import axios from "axios";
import VueAxios from "vue-axios";
import moment from "moment";
import VueSocketIO from "vue-3-socket.io";
import { createI18n } from "vue-i18n";
import messages from "./locales";

const app = createApp(App);

if (process.env.NODE_ENV === "development") {
    const { worker } = require("./mocks/browser");
    worker.start();
} else {
    axios.defaults.baseURL = process.env.VUE_APP_API_URL;
}

if (!!process.env.VUE_APP_SOCKET_URL) {
    const socketIO = new VueSocketIO({
        debug: true,
        connection: process.env.VUE_APP_SOCKET_URL,
        vuex: {
            store,
            actionPrefix: "SOCKET_",
            mutationPrefix: "SOCKET_",
        },
        options: {},
    });
    app.use(socketIO);
}

const i18n = createI18n({
    locale: "en",
    fallbackLocale: "en",
    messages,
});

app.config.globalProperties.$moment = moment;
app.use(store)
    .use(router)
    .use(VueAxios, axios)
    .use(i18n)
    .use(VueCookies, {})
    .mount("#app");
