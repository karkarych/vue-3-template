import { rest } from "msw";

export default [
    rest.get("/message", (req, res, ctx) => {
        return res(
            ctx.json({
                message: "And this message is fetched from Mock API",
            })
        );
    }),
];
